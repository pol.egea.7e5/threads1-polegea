﻿namespace Threads1_PolEgea
{
    using System;
    using System.IO;
    using System.Threading;
    class Program
    {
        static void Main(string[] args)
        {
            Thread thread1 = new Thread(new ThreadStart(() => CountLines("exemple1.txt")));
            Thread thread2 = new Thread(new ThreadStart(() => CountLines("exemple2.txt")));
            Thread thread3 = new Thread(new ThreadStart(() => CountLines("exemple3.txt")));
            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread1.Join();
            thread2.Join();
            thread3.Join();
        }

        static void CountLines(string fileName)
        {
            int count = 0;
            using (StreamReader reader = new StreamReader("../../../"+fileName))
            {
                while (reader.ReadLine() != null)
                {
                    count++;
                }
            }
            Console.WriteLine("El fitxer {0} té {1} línies.", fileName, count);
        }
    }

}
